public class NotPass {
    public static void main(String[] args) {
        String pass="pass word";
        System.out.println("pass: "+pass);
        System.out.println("is: "+checkPass(pass));
    }

    public static String checkPass(String pass){
        if(pass.length()<6){    // check if the password is shorter than 6 characters
            return "invalid";
        }
        boolean[] tests = new boolean[5];
        for(boolean b: tests){
            b=false;
        }

        int strCounter=0;
        char[] temp=pass.toCharArray();
        if(tests[3]==false && temp.length>=8){    // check if the password is at least 8 characters long
            tests[3]=true;
            strCounter++;
        }


        for(char c: temp){
            if(c==' '){
                return "invalid";
            }
            if(tests[0]==false && Character.isUpperCase(c)){   // check if there is an uppercase character
                tests[0]=true;
                strCounter++;
            }
            if(tests[1]==false && Character.isLowerCase(c)){    // check if there is an uppercase character
                tests[1]=true;
                strCounter++;
            }
            if(tests[2]==false && "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~".indexOf(c)!=-1){    // check if there is a special character
                tests[2]=true;
                strCounter++;
            }
            if(tests[4]==false && "123456789".indexOf(c)!=-1){    // check if there is a number
                tests[4]=true;
                strCounter++;
            }
        }
        System.out.println("Has upper case: "+tests[0]+"\nHas lower case: "+tests[1]+"\nHas special: "+tests[2]+"\nMinimum 8: "+tests[3]+"\nHas number: "+tests[4]);


        switch (strCounter){
            case 1: case 2:
                return "weak";
            case 3: case 4:
                return "moderate";
            case 5:
                return "strong";
            default:
                return "invalid";
        }
    }
}
